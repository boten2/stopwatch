var server = require('json-server');

server.low.db = {
    tasks: [
        {"title": 'title', "description":"description", "completed" :"completed"}
    ]
};

server.listen(3000, function () {
    console.log('======= > API server  listen on port 3000');
});