/**
 * Created by nadav on 14/08/2014.
 */


(function(){

    function sandMainCtrl(scope,dataService){
        this.todoItem =
            {'done': false };
        this.listStatus = false;
        this.myName = 'father scope is on';
        this.options = {interval: 1000, log: []};

        //scope.$watch(this.done,this.doneWasClicked);

        this.showLog = true;
        this.edit= false;
        this.fullTodoList= [];
        this.filterTodoList =this.fullTodoList;

        this.addItem = function(){
            if (this.edit.mode){
                this.edit.item.title = this.todoItem.title;
                this.edit.item.des = this.todoItem.des;
                this.todoItem = {};
                this.edit.mode= false;
                scope.$broadcast('addlog','item was edit');
            }
            else {
                this.fullTodoList.push({
                    'done': false,
                    'title': this.todoItem.title,
                    'des': this.todoItem.des,
                    'edit': false
                })

                this.todoItem = {};
                scope.$broadcast('addlog', 'new item was added');
            }
        }


           this.removeItem = function(index){
                  if (index > -1) {
                   this.fullTodoList.splice(index, 1);
                   scope.$broadcast('addlog','item was removed');
               }
           }

        this.editItem = function(item){
            this.todoItem.title = item.title;
            this.todoItem.des = item.des;
            this.edit = { 'mode':true , 'item': item};


        }
        this.changeLIstView = function(status) {
            // ** data = false / true
            if (status) {
                // ** true = show all
                this.filterTodoList = this.fullTodoList.filter(function (item) {
                    //console.log(item);
                    return item.done === false;
                });
            }
            else {
                this.filterTodoList = this.fullTodoList;
            }
        }

        this.clearLog =function(){

            scope.$broadcast('clearLog');
        }

    }

    function sandLogCtrl(scope){

      this.show = true;

      this.LogList= [];

      this.log = {};

      this.addLog= function(log){
          this.LogList.push(log);

      }
      scope.$on('addlog',function(event,data){
           //console.log(new Date());
          this.addLog({'title':data, 'time':new Date()});
      }.bind(this));


        scope.$on('clearLog',function(event,data){
            this.LogList= [];
        }.bind(this));


    }

    function sandTodoListCtrl(scope){
         //TODO what should i put here ?

    }

    function sandAddCtrl(scope){
        //TODO what should i put here ?

    }


 angular.module("sandMain")
    .controller("sandMainCtrl",["$scope",'dataService',sandMainCtrl])
    .controller("sandLogCtrl",["$scope",sandLogCtrl])
    .controller("sandTodoListCtrl",["$scope",sandTodoListCtrl])
    .controller("sandAddCtrl",["$scope",sandAddCtrl])


}());