/**
 * Created by nadav.lichtenstein on 14/08/2014.
 */


angular
    .module('sandMain', [
        'ngRoute',
        'StopWatch'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'sand_main/sand_main.html',
                controller: 'sandMainCtrl',
                controllerAs : 'main'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
