/**
 * Created by nadavv on 9/6/14.
 */

/*
 * The Stopwatch module consists of one directives, one controller, and one filter.
 */
angular.module('StopWatch', [])
    .controller('stopwatchCtrl', ['$scope', 'StopwatchFactory',    function($scope, StopwatchFactory){
        //console.log(this);
        this.stopwatchService = new StopwatchFactory($scope.options);
        this.myName ='directive scope is on';

    }])
    .filter('stopwatchTime', function () {
        return function (input) {
            if(input){
                var elapsed = input.getTime();
                var hours = parseInt(elapsed / 3600000,10);
                elapsed %= 3600000;
                var mins = parseInt(elapsed / 60000,10);
                elapsed %= 60000;
                var secs = parseInt(elapsed / 1000,10);
                var ms = elapsed % 1000;

                return hours + ':' + mins + ':' + secs + ':' + ms;
            }
        };
    })

// Version 3
    .directive('bbStopwatch', function(){
        return {
            restrict: 'EA',
            scope: {options: '='},
            controller: 'stopwatchCtrl',
            // controllerAs you can write watch.myname on the html ! file
            controllerAs: 'watch',
            /*
            transclude here is very important because in the html of the directive
            we have child nodes : all inside the first div :

             <div bb-stopwatch options="main.options" override=true>
             <div class="container">
             <div class="stopWatch numbers">
             {{options.elapsedTime | stopwatchTime}}
             </div>

            child->    <button class="btn" ng-click="startTimer()">Start</button>
             <button class="btn" ng-click="stopTimer()">Stop</button>
             <button class="btn" ng-click="resetTimer()">Reset</button>
             {{main.myName}}{{watch.myName}}
             </div>

             </div>

             unfortunately, the isolated scope that we define on the bbStopwatch
             wont inherit to the child nodes -- so the child will get
             the parent scope+controller = main.ctrl
             but, if we use transclude we can take in the compile stage all
             the child nodes and associate them with the bbStopwatch scope :

             transclude(scope,transcludeFn);

             you can see the full code underneath :

             */
            transclude: true,
            compile: function(tElem, tAttrs){

                if (!tAttrs.options){
                    throw new Error('Must Pass an options object from the Controller For the Stopwatch to Work Correctly.');
                }

                var overrideScope = tAttrs.override;
//                console.log('here');
//                console.log(overrideScope);

                return function(scope, elem, attrs, controller, transclude){

                    function transcludeFn(clone){
                        elem.append(clone);
                    }
                    //console.log(this);
                    scope.startTimer = controller.stopwatchService.startTimer;
                    scope.stopTimer = controller.stopwatchService.stopTimer;
                    scope.resetTimer = controller.stopwatchService.resetTimer;

                    if(overrideScope=== 'true'){
                        console.log('true');
                        transclude(scope,transcludeFn);
                    } else {
                        console.log('false');
                        transclude(transcludeFn);
                    }

                    scope.$on('$destroy', function(node){
                        controller.stopwatchService.cancelTimer();
                    });
                };
            }
        };
    })

// factory function
    .factory('StopwatchFactory', ['$interval',    function($interval){

        return function(options){

            var startTime = 0,
                currentTime = null,
                offset = 0,
                interval = null,
                self = this;

            if(!options.interval){
                options.interval = 100;
            }

            options.elapsedTime = new Date(0);

            self.running = false;

            function pushToLog(lap){
                if(options.log !== undefined){
                    options.log.push(lap);
                }
            }

            self.updateTime = function(){
                currentTime = new Date().getTime();
                var timeElapsed = offset + (currentTime - startTime);
                options.elapsedTime.setTime(timeElapsed);
            };

            self.startTimer = function(){
                console.log('start time');
                if(self.running === false){
                    startTime = new Date().getTime();
                    interval = $interval(self.updateTime,options.interval);
                    self.running = true;
                }
            };

            self.stopTimer = function(){
                if( self.running === false) {
                    return;
                }
                self.updateTime();
                offset = offset + currentTime - startTime;
                pushToLog(currentTime - startTime);
                $interval.cancel(interval);
                self.running = false;
            };

            self.resetTimer = function(){
                startTime = new Date().getTime();
                options.elapsedTime.setTime(0);
                timeElapsed = offset = 0;
            };

            self.cancelTimer = function(){
                $interval.cancel(interval);
            };

            return self;

        };


    }]);