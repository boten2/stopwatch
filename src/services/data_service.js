'use strict';

/**
 * @ngdoc service
 * @name yoApp.DataService
 * @description
 * # DataService
 * Service in the yoApp.
 */
(function(){

    function DataService(){

        this.getData = function(){
            var list = [
                     {'done': false , 'title': 'buy milk', 'des': 'buy tomorrow'}
            ];
            return list;
        }

    }


    angular.module("sandMain")
        .service("dataService",[DataService]);





}());
